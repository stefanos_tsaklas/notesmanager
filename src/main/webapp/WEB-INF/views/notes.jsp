<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Create and view notes">
<meta name="author" content="Tipico">
<link rel="icon" href="../../favicon.ico">

<title>Manage notes</title>

<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="resources/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/notesTheme.css" rel="stylesheet">

<!-- Ng-grid style -->
<link rel="stylesheet" type="text/css"
	href="resources/bower_components/ng-grid/ng-grid.css" />

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!--  Load angular library, and the code for our own custom controller -->
<script data-require="angular.js@1.3.0" data-semver="1.3.0"
	src="https://code.angularjs.org/1.3.0/angular.js"></script>
<script src="resources/js/notesController.js"></script>

<!-- ng-grid -->
<script src="resources/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript"
	src="resources/bower_components/ng-grid/ng-grid-2.0.14.min.js"></script>

</head>

<body role="document">

	<!-- Fixed navbar -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="">Tipico</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="home.htm">Home</a></li>
					<li class="active"><a href="notes.htm">Notes</a></li>
					<li><a href="comments.htm">Comments</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container theme-showcase" role="main" ng-app="notesApp"
		ng-controller="notesController">

		<!-- jumbotron to explain the meaning of the page -->
		<div class="jumbotron" style="height: 10px;">
			<p>Use this page to create and view notes in a second.</p>
		</div>

		<!-- The input to create new notes -->
		<div class="page-header">
			<h1>Create</h1>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="container">
					<div class="alert alert-success" role="alert"
						ng-show="alertMessageStatus==1">
						<strong>Well done!</strong> A new note was just created!
					</div>
					<div class="alert alert-danger" role="alert"
						ng-show="alertMessageStatus==-1">
						<strong>Error!</strong> Failed to create new note.
					</div>
					<div class="alert alert-warning" role="alert"
						ng-show="noteForm.noteInput.$dirty && noteForm.noteInput.$error.required">
						<strong>Warning! </strong> You can't create a note with no text.
					</div>
					<div class="alert alert-warning" role="alert"
						ng-show="noteForm.noteInput.$error.maxlength">
						<strong>Warning! </strong> Sorry, your note is longer than 100 characters.
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="container">
					<form class="form-inline" role="form" name="noteForm">
						<div class="form-group">
							<label class="sr-only" for="noteInput">Note:</label> 
							<input id="noteInput" name="noteInput" type="text" class="form-control"
								placeholder="create a new note" ng-model="textOfNewNote"
								required ng-maxlength="100">
						</div>
						<button type="submit" class="btn btn-default"
							ng-click="saveNewNote()"
							ng-disabled="noteForm.noteInput.$dirty && noteForm.noteInput.$invalid">Send</button>
						<p ng-show="noteForm.$valid">({{100 - textOfNewNote.length}} characters left)</p> 
					</form>
				</div>
			</div>
		</div>

		<!-- The table to view past notes -->
		<div class="page-header">
			<h1>View</h1>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="gridStyle" ng-grid="gridOptions"></div>
			</div>
		</div>

	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<!-- script src="resources/js/docs.min.js" script -->
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="resources/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>