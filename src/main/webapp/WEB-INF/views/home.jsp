<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
	<title>Assessment Project - Java, Spring MVC, AngularJS</title>
	<meta charset="UTF-8">
</head>
<body>
<h1>SD Assessment Project - Java, Spring MVC, AngularJS</h1>

	<a href="notes.htm">NEW: Click here to manage your notes! </a>
	<hr/>
	
<p style="width:600px;">
    Welcome to the Tipico SD Assessment Project - Java, Spring MVC, AngularJS<br /><br>
    Here you find a short summary of your tasks.
    Please use this project "sd_assessment" as your skeleton and add new sources to complete the tasks.
    Follow the <b>Development and Submission steps</b> in order to do the below <b>Tasks</b> and submit them to us for review.
</p>

<h2>Tasks</h2>

Create a new site where users are able to:

<ol>
    <li>Create notes via a web based page having the ability to create a new note through an input field.</li>
    <li>Notes can be submitted to the server by clicking a button "Send" which does its work by utilizing AngularJS.</li>
    <li>All created notes are to displayed below the input field, sorted by date.</li>
    <li>Notes can be sorted ascending or descending by its creation date.</li>
    <li>Notes have a max length of 100 characters.</li>
    <li>Notes must be saved on the server (mySQL DB, schema titled TIPICO) so that they can survive a reboot.</li>
    <li>Unit tests should be available to test saving and loading notes as well as any other logic you see worth testing (Java and Angular).</li>
    <li>The procedure of saving and loading a note should take place in a spring bean (eg. a service bean).</li>
    <li>The list of notes below the input field should refresh to reflect changes.</li>
    <li>The list of created notes should be pageable, showing 10 notes at a time, with the ability to view other pages in case there are more than 10 notes.</li>
</ol>
<br>

<h2>Development and Submission steps</h2>

<ol>
    <li>Clone the master branch to your local machine</li>
    <li>Please commit frequently to your local git repository (no need to push)</li>
    <li>When ready, make sure everything is committed and clean the target folder</li>
    <li>Add any assumptions or notes to the file named COMMENTS.md</li>
    <li>Send us a zip version of the folder via email</li>
</ol>

<br>
<strong>Good luck!</strong>
</body>
</html>
