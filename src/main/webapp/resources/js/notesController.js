var myApp = angular.module('notesApp', ['ngGrid']);

function NotesController($scope, $http, $timeout) {

	//used to display a message above the Send button
	$scope.alertMessageStatus = 0;

	//total number of notes stored in the database
	$scope.totalServerItems = 0;

	//pagination parameters, default size of a page is 10 records
	$scope.pagingOptions = {
			pageSizes: [10],
			pageSize: 10,
			currentPage: 1
	};
	
	//use this object to sort in a specific field, default is descending date of creation
	$scope.sortInfo = { fields: ['creationDate'], directions: ['desc']};

	//definitions of the 3 columns of the table
	$scope.columnDefs = [
	                      { field: "id", width:"10%"},
	                      { field: "text", width:"70%"},
	                      { field: "creationDate", width:"20%",
	                    	  displayName: "date",
	                    	  cellFilter: 'date:\'dd/MM/yyyy HH:mm:ss \'' 
	                      }
	];
	
	$scope.gridOptions = {
			data: 'myData',
			enablePaging: true,
			showFooter: true,
			totalServerItems: 'totalServerItems',
			pagingOptions: $scope.pagingOptions,
			sortInfo:      $scope.sortInfo,
			columnDefs:    $scope.columnDefs,
	};

	//update paging data
	$scope.setPagingData = function(data, page, pageSize){	
		var pagedData = data.content;
		$scope.myData = pagedData;

		$scope.totalServerItems = data.totalElements; 
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	};

	//update table of results
	$scope.updateTable = function(){
		$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.sortInfo.fields[0], $scope.sortInfo.directions);
	}

	//get page of results by calling a REST service
	$scope.getPagedDataAsync = function (pageSize, page, sortField, sortOrder) {
		setTimeout(function () {

			$http.get('http://localhost:8080/assessment/rest/v1/pagedNotes?pageNumber=' + (page-1) + '&pageSize=' + pageSize + '&sortField=' + sortField +"&sortOrder=" + sortOrder)
			.success(function (results) {
				$scope.setPagingData(results, page, pageSize);				
			});

		}, 1000);
	};

	//update table if paging options change (only current page may change, page size is always 10)
	$scope.$watch('pagingOptions', function (newVal, oldVal) { 
		if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
			$scope.updateTable();
		}
	}, true);

	//update table if sorting options change
	$scope.$watch('sortInfo', function (newVal, oldVal) { 
		if (newVal !== oldVal) {
			$scope.updateTable();
		}
	}, true);

	//saves a new note, by POSTing to the REST service
	$scope.saveNewNote = function(){

		$http({
			method: "POST",
			url:  "http://localhost:8080/assessment/rest/v1/note",
			data: JSON.stringify( { "text": $scope.textOfNewNote } ),
			contentType: "application/json; charset=utf-8",
			dataType: "json"			
		})
		.then(function(response) { //success
			$scope.updateTable();
			$scope.alertMessageStatus = 1;
		}, 
		function(response) { //failure
			$scope.alertMessageStatus = -1;
		});

		//hide message (of success or failure), after 3 seconds
		$timeout(function() {
			$scope.alertMessageStatus = 0;
		}, 3000);

	}

	//gets all available notes, without pagination, by a GET request to the REST service
	function getResults() {

		$http.get('http://localhost:8080/assessment/rest/v1/notes')
		.then(function(res) {
			$scope.results = [];
			$scope.results = res.data;
		});

	}
	
	//update table of results
	$scope.updateTable();

}

myApp.controller('notesController', NotesController);