package com.tipico.assessment.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tipico.assessment.domain.Note;
import com.tipico.assessment.service.NoteService;

@RequestMapping("/rest/v1")
@RestController
public class NoteRestController {
 
	private final static Logger LOG = LoggerFactory.getLogger(NoteRestController.class);

    @Autowired
    private NoteService noteService;
    
    @RequestMapping(method = RequestMethod.POST, value = "/note", consumes = {"application/json"})
    public ResponseEntity<Note> update(@RequestBody Note note) {
    	
    	LOG.debug("POST note, for creating new note {} ", note.toString());
    	
    	//not allowing to create notes without text
    	if(note.getText()==null || note.getText().length()==0)
    	{
    		LOG.debug("Not allowing to create a note without text");
    		return new ResponseEntity<Note>(HttpStatus.BAD_REQUEST);
    	}
    	
    	//not allowing creation of notes longer than 100 characters   
    	if(note.getText()!=null && note.getText().length()>100)
    	{
    		LOG.debug("Not allowing to create a note with {} characters", note.getText().length());
    		return new ResponseEntity<Note>(HttpStatus.PAYLOAD_TOO_LARGE);
    	}
    	
    	Note createdNote =  this.noteService.createNote(note);
        return new ResponseEntity<Note>(createdNote, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/notes", produces = {"application/json"})
    public ResponseEntity<List<Note>> getNotes() {
       	
    	LOG.debug("GET notes, for getting all notes ");
    	List<Note> allNotes =  this.noteService.getAllNotes();
        return new ResponseEntity<List<Note>>(allNotes, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/pagedNotes", produces = {"application/json"})
    public  Page<Note> getPageOfNotes(	@RequestParam(value="pageNumber", defaultValue="0") int pageNumber, 
    								 	@RequestParam(value="pageSize",   defaultValue="10") int pageSize, 
    								 	@RequestParam(value="sortField",  defaultValue="creationDate")String sortField,
    								 	@RequestParam(value="sortOrder",  defaultValue="ASC")String sortOrder ) {

    	LOG.debug("GET pagedNotes, requested page {} with {} records", pageNumber, pageSize);
    	Page<Note>  notes = this.noteService.getPageOfNotes(pageNumber, pageSize, sortField, Sort.Direction.fromString(sortOrder));
 
    	LOG.debug("GOT back {} pages, total number of records is {}", notes.getSize(), notes.getTotalElements());
    	return notes;
    }
    
}