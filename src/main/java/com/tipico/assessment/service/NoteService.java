package com.tipico.assessment.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import com.tipico.assessment.domain.Note;

public interface NoteService {

	Note		createNote(Note n);
	List<Note>	getAllNotes();
	Page<Note>	getPageOfNotes(int pageNumber, int pageSize, String sortField, Sort.Direction sortOrder);

}
