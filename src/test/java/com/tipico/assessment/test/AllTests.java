package com.tipico.assessment.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.tipico.assessment.test.repository.NoteRepositoryUnitTest;
import com.tipico.assessment.test.service.NoteServiceIntegrationTest;

@RunWith(Suite.class)
@SuiteClasses({NoteServiceIntegrationTest.class, NoteRepositoryUnitTest.class})
public class AllTests {

	//NOTE: unfortunately you have to remove sl4j dependencies from classpath to run the test suite
	//please modify pom.xml, or remove the sl4j jars to run the test suite
}

