package com.tipico.assessment.test.service;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hamcrest.collection.IsEmptyIterable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tipico.assessment.domain.Note;
import com.tipico.assessment.service.NoteServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ComponentScan
@ContextConfiguration("file:src/test/resources/test-root-context.xml")
public class NoteServiceIntegrationTest {

	@Autowired
	protected NoteServiceImpl noteServiceUnderTest;
	
	@Test
	public void shouldSaveNewNoteAndFindLatestNoteWithTheSameId() {

		//create unique random text for note
		String uniqueText = UUID.randomUUID().toString();

		//save a new note in the database, get the returned object (it must contain the generated id)
		Note newNote = this.noteServiceUnderTest.createNote(new Note(-1, uniqueText, new Date()));
		//get the latest note from the database, it must be the note we saved above
		Page<Note> latestNotes = this.noteServiceUnderTest.getPageOfNotes(0, 1, "creationDate", Direction.DESC);

		//the page must contain just 1 result
		assertThat("Page must contains just 1 note", latestNotes.getSize(), is(1));
		//the note must have a new id, not -1
		assertThat("Id of note must not be -1", latestNotes.getContent().get(0).getId(), not(is((long)-1)) );
		//the object we got back from the getPageOfNotes must have the same text with the object we got back from createNewNote, and it must be equal to uniqueText
		assertThat("The text returned by getPageOfNotes must be the same with the one returned by createNote, and the one we generated", latestNotes.getContent().get(0).getText(), allOf( is(newNote.getText()), is(uniqueText)) );		
	}

	@Test
	public void shouldSaveVeryLongNoteText() {

		//create a very long text to save
		String longText =  new String(new char[150]).replace('\0', 'a');

		//save a new note in the database, get the returned object (it must contain the generated id)
		Note newNote = this.noteServiceUnderTest.createNote(new Note(-1, longText, new Date()));
		
		//the note must have been saved with no problems (it is the REST service that refuses to save notes longer than 100 characters)
		assertThat("The note must have the text we requested persist", newNote.getText(), is(longText));
	}
	
	@Test
	public void testPaginationReturnsAllStoredNotes() {

		//save 13 new notes
		final int numberOfNotesToCreate = 13;
		final int pageSize = 3;
		for(int i=0; i<numberOfNotesToCreate; i++)
			this.noteServiceUnderTest.createNote(new Note(-1, "some text, doesn't matter", new Date()));

		//get all notes of database
		List<Note> notesList = this.noteServiceUnderTest.getAllNotes();

		//get all notes with pagination, in pages of 3
		List<Note>	pagedNotesList = new ArrayList<Note>();
		for(int i=0; i< (notesList.size()/pageSize)+1; i++)
		{
			Page<Note> notesPage = this.noteServiceUnderTest.getPageOfNotes(i, pageSize, "id", Direction.ASC);
			pagedNotesList.addAll(notesPage.getContent());
		}

		//the size of the two lists must be the same
		assertThat("Pagination must return all notes from database", pagedNotesList.size(), is(notesList.size()));	
	}

	@Test
	public void testAscendingOrderOfId() {

		//save 50 new notes
		for(int i=0; i<50; i++)
			this.noteServiceUnderTest.createNote(new Note(-1, "some text, doesn't matter", new Date()));

		//get all notes in one page, in ascending order of id
		Page<Note> hugePageWithNotes = this.noteServiceUnderTest.getPageOfNotes(0, Integer.MAX_VALUE, "id", Direction.ASC);

		//check that the notes are in ascending order of ids
		for(int i=1; i<hugePageWithNotes.getContent().size(); i++)
		{	
			long currentId  = hugePageWithNotes.getContent().get(i).getId();
			long previousId = hugePageWithNotes.getContent().get(i-1).getId();
			assertThat("Notes must be in ascending order of id", currentId, greaterThan(previousId));
		}
	}

	@Test
	public void testDescendingOrderOfId() {

		//save 50 new notes
		for(int i=0; i<50; i++)
			this.noteServiceUnderTest.createNote(new Note(-1, "some text, doesn't matter", new Date()));

		//get all notes in one page, in descending order of id
		Page<Note> hugePageWithNotes = this.noteServiceUnderTest.getPageOfNotes(0, Integer.MAX_VALUE, "id", Direction.DESC);

		//check that the notes are in ascending order of ids
		for(int i=1; i<hugePageWithNotes.getContent().size(); i++)
		{	
			long currentId  = hugePageWithNotes.getContent().get(i).getId();
			long previousId = hugePageWithNotes.getContent().get(i-1).getId();
			assertThat("Notes must be in descending order of id", currentId, lessThan(previousId));
		}
	}

	@Test
	public void testAscendingOrderOfText() {

		//save 50 new notes
		for(int i=0; i<50; i++)
			this.noteServiceUnderTest.createNote(new Note(-1, UUID.randomUUID().toString(), new Date()));

		//get all notes in one page, in ascending order of text
		Page<Note> hugePageWithNotes = this.noteServiceUnderTest.getPageOfNotes(0, Integer.MAX_VALUE, "text", Direction.ASC);

		//check that the notes are in ascending order of text
		for(int i=1; i<hugePageWithNotes.getContent().size(); i++)
		{	
			String currentText  = hugePageWithNotes.getContent().get(i).getText();
			String previousText = hugePageWithNotes.getContent().get(i-1).getText();
			assertThat("Notes must be in ascending order of text", currentText, greaterThan(previousText));
		}
	}

	@Test
	public void testDescendingOrderOfDate() {

		//save 50 new notes
		for(int i=0; i<50; i++)
			this.noteServiceUnderTest.createNote(new Note(-1, UUID.randomUUID().toString(), new Date()));

		//get all notes in one page, in ascending order of text
		Page<Note> hugePageWithNotes = this.noteServiceUnderTest.getPageOfNotes(0, Integer.MAX_VALUE, "text", Direction.DESC);

		//check that the notes are in descending order of text
		for(int i=1; i<hugePageWithNotes.getContent().size(); i++)
		{	
			String currentText  = hugePageWithNotes.getContent().get(i).getText();
			String previousText = hugePageWithNotes.getContent().get(i-1).getText();
			assertThat("Notes must be in descending order of text", currentText, lessThan(previousText));
		}
	}

	@Test
	public void testAscendingOrderOfCreationDate() {

		//save some new notes (optional, set to 0 to discard)
		final int numberOfNewNotes = 0;
		for(int i=0; i<numberOfNewNotes; i++)
		{
			try {
				//stall for 1 second to insert records with different timestamps
				Thread.sleep(1100);
			} catch (InterruptedException e) {
				return;
			}
			this.noteServiceUnderTest.createNote(new Note("not important"));
		}

		//get all notes in one page, in ascending order of date
		Page<Note> hugePageWithNotes = this.noteServiceUnderTest.getPageOfNotes(0, Integer.MAX_VALUE, "creationDate", Direction.ASC);

		//check that the notes are in ascending order of text
		for(int i=1; i<hugePageWithNotes.getContent().size(); i++)
		{	
			Date currentDate  = hugePageWithNotes.getContent().get(i).getCreationDate();
			Date previousDate = hugePageWithNotes.getContent().get(i-1).getCreationDate();
			assertThat("Notes must be in ascending order of text", currentDate, greaterThanOrEqualTo(previousDate));
		}
	}

	@Test
	public void testDescendingOrderOfCreationDate() {

		//save some new notes (optional, set to 0 to discard)
		final int numberOfNewNotes = 0;
		for(int i=0; i<numberOfNewNotes; i++)
		{
			try {
				//stall for 1 second to insert records with different timestamps
				Thread.sleep(1100);
			} catch (InterruptedException e) {
				return;
			}
			this.noteServiceUnderTest.createNote(new Note("not important"));
		}

		//get all notes in one page, in descending order of date
		Page<Note> hugePageWithNotes = this.noteServiceUnderTest.getPageOfNotes(0, Integer.MAX_VALUE, "creationDate", Direction.DESC);

		//check that the notes are in ascending order of text
		for(int i=1; i<hugePageWithNotes.getContent().size(); i++)
		{	
			Date currentDate  = hugePageWithNotes.getContent().get(i).getCreationDate();
			Date previousDate = hugePageWithNotes.getContent().get(i-1).getCreationDate();
			assertThat("Notes must be in descending order of text", currentDate, lessThanOrEqualTo(previousDate));
		}
	}

	@Test(expected=Exception.class)
	public void shouldFailToSortInUnknownFieldName() {

		//save 3 new notes
		for(int i=0; i<3; i++)
			this.noteServiceUnderTest.createNote(new Note(-1, UUID.randomUUID().toString(), new Date()));

		//try to get paged results sorted on unknown field
		this.noteServiceUnderTest.getPageOfNotes(0, 2, "this-is-a-non-existing-field", Direction.ASC);
		
		//must have thrown an exception
		fail("The above call should have thrown an exception");
	}
	
	@Test(expected=Exception.class)
	public void shouldThrowExceptionForNegativeIndex() {

		//try to get a page out of bounds
		this.noteServiceUnderTest.getPageOfNotes( -1, 10, "id", Direction.ASC);
		
		//must have thrown an exception
		fail("The above call should have thrown an exception");
	}
	
	@Test
	public void shouldReturnEmptyListForOutOfBoundsIndex() {

		//get all notes
		List<Note> notesList = this.noteServiceUnderTest.getAllNotes();
		
		//try to get a page out of bounds
		Page<Note> pagedNotes = this.noteServiceUnderTest.getPageOfNotes( 3*notesList.size(), 1, "id", Direction.ASC);
		
		//that page must be empty
		assertThat("The list must be empty", pagedNotes.getContent(), IsEmptyIterable.emptyIterable() );
		assertThat("The size must be 0", pagedNotes.getNumberOfElements(), is(0));
	}

}