package com.tipico.assessment.test.repository;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tipico.assessment.dao.NoteRepository;
import com.tipico.assessment.domain.Note;

@RunWith(SpringJUnit4ClassRunner.class)
@ComponentScan
@ContextConfiguration("file:src/test/resources/test-root-context.xml")
public class NoteRepositoryUnitTest {

	@Autowired
	NoteRepository noteRepositoryUnderTest;
	
	@Before
	public void setUp() throws Exception {

		//clean database before every unit test
		this.noteRepositoryUnderTest.deleteAll();
	}

	@Test
	public void shouldReturnNoRecordsInEmptyDatabase() {

		//get count of records
		long count = this.noteRepositoryUnderTest.count();
		
		//should return 0 records
		assertThat("An empty database should have 0 records", count, is((long)0));	
	}
	
	@Test
	public void shouldInsertExactlyKRecords() {

		//insert k records
		int k = 21;
		for(int i=0; i<k; i++)
			this.noteRepositoryUnderTest.save(new Note("doesn't matter"));
		ArrayList<Note> allNotes = (ArrayList<Note>) this.noteRepositoryUnderTest.findAll();
		
		//should return k records
		assertThat("Database should have " + " records ", allNotes.size(), is(k));	
	}
	
	@Test
	public void shouldSaveNote() {

		//insert a note
		String uniqueText = UUID.randomUUID().toString();
		long primaryKey = this.noteRepositoryUnderTest.save(new Note(uniqueText)).getId();
		
		//get a note from database
		Note noteFromDatabase = this.noteRepositoryUnderTest.findOne(primaryKey);
		
		//the note must contain the id that we got when saving
		assertThat("Record should have the primary key we specified", noteFromDatabase.getId(), is(primaryKey));			
		//the note must contain the text that we specified
		assertThat("Record should contain the text we tried to save", noteFromDatabase.getText(), is(uniqueText));	
	}
}