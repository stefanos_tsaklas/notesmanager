# Assessment Project Comments And Assumptions

Please write down any comments or assumptions you would like us to know when reviewing the test in the list below.

## Instructions to run

	- Please fill in your MySQL credentials in file src\main\resources\application.properties
	- Just deploy the project in the app server running at port 8080 (we used Pivotal tc)
	- Please use port 8080
	- Please update pom.xml if you want to run the test suite, you have to remove slf4j jars from classpath
	- You can use tipico_schema.sql to create database and tipico_data.sql to populate the table with some records
	- Browse http://localhost:8080/assessment/notes.htm
	
## Comments And Assumptions

	The project contains a decent solution for the requirements.
	It showcases some good practices of abstraction through the layers 
	of the backend, as well as use of cutting-edge Spring libraries,
	and the use of ng-grid and Bootstrap for the front end. 
	
	The project works exactly as requested, the only thing that 
	could be improved is adding more unit tests (especially on 	NoteRestController), and I have not worked with testing 
	frameworks for the front-end before, so such tests are missing.
	Please read TODO list below for more improvements.
	
	FRONT-END:
		The html code is in notes.jsp, which includes the angular 		controller code src\main\webapp\resources\js\notesController.js
		The controller communicates with the back-end, to create and get 		notes, using http calls to a REST service
		The controller uses ng-grid for pagination, which is of course
		server-side pagination, to tackle scalability of a huge dataset.
			
	BACK-END 
		NoteRestController accepts and returns JSON objects over HTTP
		It delegates most of the work to NoteServiceImpl, which is an
		abstraction over the "very powerful with no boilerplate code"
		NoteRepository, which is a full-blown dao provided by 
		Spring DATA JPA. The only thing NoteRestController adds
	 	to NoteServiceImpl is to check for length of new notes, while 
	 	NoteServiceImpl can save notes of any length, and it also 
	 	returns JSON responses to the client, instead of POJOs,
	 	thus we believe this abstraction is essential.
	 	
	 REST
		 
	 	1) Creating a new note is a POST request 
	 	that contains the next text
		---------------------------------------	 
	 	POST /assessment/rest/v1/note HTTP/1.1
		Host: localhost:8080
		Content-Type: application/json
		Cache-Control: no-cache

		{ "text":"This is a new note"}
		---------------------------------------



		Which returns the new resource that was created
		---------------------------------------	 		
		{"id":3,"text":"This is a new note","creationDate":1422277756647}
		---------------------------------------	 

		2) Gettin the first page of 5 notes in each page, 
		in ascending order of creation date
		---------------------------------------	 
	 	GET assessment/rest/v1/pagedNotes?pageNumber=0&pageSize=5&sortField=id&sortOrder=desc
	 
		---------------------------------------
		Which returns a JSON with all the notes in that page
		
				{"content":[{"id":31,"text":"Note 31","creationDate":1294037101000},{"id":30,"text":"Note 30","creationDate":1413056053000},{"id":29,"text":"Note 29","creationDate":1410551063000},{"id":28,"text":"Note 28","creationDate":1403553284000},{"id":27,"text":"Note 27","creationDate":1398368675000}],"totalPages":7,"totalElements":31,"last":false,"size":5,"number":0,"first":true,"sort":[{"direction":"DESC","property":"id","ignoreCase":false,"nullHandling":"NATIVE","ascending":false}],"numberOfElements":5}
		
		
## TODO LIST

	- Add more comprehensive tests, e.g. tests for NoteRestController are missing
	- Could not add tests for NoteRestController due to configuration error, I believe I have to upgrade to servlet 3.0.1 to achieve this, and 
	my dependencies were not compatible with this decision. 
	- Add tests for the front-end, examine appropriate tools for this purpose, as I don't have any experience in front end testing frameworks.
	- Fix the dependency on slf4j, right now we have to modify pom.xml to exclude those jars in order to run test suite.
	- The header of the site is duplicated across the different files, only one copy should be included by any file that need it.
	- Externalise hardcoded database info from .xml to application.properties
	- Decide if resources will be local or loaded from external CDN, e.g. we are using loading https://code.angularjs.org/1.3.0/angular.js
	- We have commited the whole bower_components folder in source control, maybe this could be done with something like pom.xml
	- The url of the rest service is hardcoded in js file, and it won't work if you don't use port 8080, we have to remove http://localhost:8080

## BUGS
	
	- The text of a note is not visible if it is too long, because rows of the table have fixed width
	
## Series of commits
1)  Initial commit, no features implemented yet 

	- Moved HomeController to its own package (com.tipico.assessment.controller)
	- Modified log4j.xml to remove errors in Eclipse Spring Tool Suite
	- Added application.properties, for future use
	- Upgraded pom.xml to use the latest Spring version (4.1.4)

2) Made a simple skeleton of a web site, but the site is not functional yet, it is static.

	- Created pages notes.jsp and comments.jsp
	- Added controller for notes (NotesController), but added a mapping for comments.htm in Homecontroller
	- Home page is now accessed by both "/" and "/home.htm"
	- Imported bootstrap framework, added in webapp/resources (folders for css, js and fonts)
	
3) Created a simple MySQL database, containing one table and two indexes.

	- Added two sql scripts in src/main/resources
	- Script tipico_schema.sql creates the schema of a database named Tipico
	- Script tipico_data.sql populates table 'note' with data, just 16 records

4) The site is functional now, the user can create a new note and view all notes, but without pagination.

	- Added mysql database details in application.properties
	- Updated pom.xml to include org.springframework.boot, org.springframework.data, mysql
	- Added Note, a domain class that represents a note object, is persisted to the database and de-serialized from/to JSON
	- Added NoteRepository, a dao that does all the database work, with no boilerplate code
	- Added NoteService and NoteServiceImpl, a service that allows us to create and get notes from the persistence layer
	- Added NoteRestController, it exposes out notes by exchanging JSON over simple HTTP requests/responses
	- Added notesController.js, the script that contains our controller logic
	- Modified notes.jsp, to connect html with the angular controller, the table now presents all notes in database
	
5)
	Notes are now presented in pages, using ng-grid. Added messages and validation of input.
	
	- Added mapping /assessment/rest/v1/pagedNotes in NoteRestController, to get paged results of notes
	- e.g. getting url "pagedNotes?pageNumber=0&pageSize=5&sortField=text&sortOrder=asc" returns the first page of 5 results, sorted in asceding order of text, in JSON format
	- Added folder resources/bower_components, it contains JS dependencies on ng-grid and jquery
	- Added method getPageOfNotes in NoteService, to get a page of results, sorted in ascending or ascending order of id, text, or creation date
	- Changed table in notes.jsp, to present paginated results using ng-grid
	- Added some messages in notes.jsp, after success or failure to send a note
	- Added validation for the input field, so that the new note is between 1 and 100 characters
	- Added some more entries in script tipico_data.sql
	
	
6) Added test suite, and final comments.

	- Added class NoteServiceIntegrationTest, which tests NoteService thoroughly, with dependency on a NoteRepository to be the real implementation, not a mock object.
	- Added class NoteRepositoryUnitTest, which contains a few basic unit tests, mainly just to test if it has been configured correctly.
	- Added test suite AllTests, all future test should be added here.
	- Added config file for unit tests src/test/resources/test-root-context.xml
	- Added comments in COMMENTS.md
	
